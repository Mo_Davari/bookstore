package com.example.bookstore;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyBookAdapter extends RecyclerView.Adapter<MyBookAdapter.ViewHolder>{
    MyBookData[] myBookData;
    Context context;


    public MyBookAdapter(MyBookData[] myBookData,MainActivity2 activity) {
        this.myBookData=myBookData;
        this.context = activity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.book_item_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MyBookData myBookDataList = myBookData[position];
        holder.textView.setText(myBookDataList.getBookName());
        holder.bookImage.setImageResource(myBookDataList.getBookImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity3.myBookData = myBookDataList;
                Intent intent1 = new Intent(context,MainActivity3.class);
                context.startActivity(intent1);
            }
        });
    }


    @Override
    public int getItemCount() {
        return myBookData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView bookImage;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bookImage = itemView.findViewById(R.id.imageview);
            textView = itemView.findViewById(R.id.textName);

        }
    }
}
