package com.example.bookstore;

public class MyBookData {
    private String bookName;
    private Integer bookImage;

    public MyBookData(String bookName, Integer bookImage) {
        this.bookName = bookName;
        this.bookImage = bookImage;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getBookImage() {
        return bookImage;
    }

    public void setBookImage(Integer bookImage) {
        this.bookImage = bookImage;
    }
}
