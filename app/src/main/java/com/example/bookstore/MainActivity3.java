package com.example.bookstore;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;

import java.util.ArrayList;
import java.util.List;


public class MainActivity3 extends AppCompatActivity {
    public static MyBookData myBookData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        TextView textView = (TextView)findViewById(R.id.textView);
        ImageSlider imageSlider=findViewById(R.id.slider);
        List<SlideModel> slideModels=new ArrayList<>();
        if(myBookData.getBookImage() == R.drawable.b1) {
        slideModels.add(new SlideModel(R.drawable.b1, ScaleTypes.CENTER_CROP));
        slideModels.add(new SlideModel(R.drawable.b12,ScaleTypes.CENTER_CROP));
        textView.setText("Description: کتاب «هنر سینما» را «دیوید بوردول»، «کریستین تامسون» نوشته و «فتاح محمدی» آن را به فارسی ترجمه کرده است. «تولید فیلم»، «فرم فیلم»، «سبک فیلم»، «نقد تحلیلی فیلم» و «تاریخ سینما» عنوان بخشهای کتاب است.");
        }else if(myBookData.getBookImage() == R.drawable.b2){
            slideModels.add(new SlideModel(R.drawable.b2, ScaleTypes.CENTER_CROP));
            slideModels.add(new SlideModel(R.drawable.b22,ScaleTypes.CENTER_CROP));
            textView.setText("Description: نمایشنامه ی «دستهای آلوده» نوشته ی «ژان پل سارتر» را «قاسم صنعوی» به فارسی برگردانده است. این نمایشنامه نخستین بار در دوم آوریل 1948 در پاریس تئاتر آنتوان به صحنه برده شد");
       } else if(myBookData.getBookImage() == R.drawable.b3){
        slideModels.add(new SlideModel(R.drawable.b3, ScaleTypes.CENTER_CROP));
        slideModels.add(new SlideModel(R.drawable.b31,ScaleTypes.CENTER_CROP));
        textView.setText("Description: کتاب گزینه اشعار سیاوش کسرایی مجموعه اشعاری از این شاعر معاصر را دربردارد.");
     }else{
        slideModels.add(new SlideModel(R.drawable.b4, ScaleTypes.CENTER_CROP));
        slideModels.add(new SlideModel(R.drawable.b41,ScaleTypes.CENTER_CROP));
        textView.setText("Description: تئوری اقتصاد خرد روشهای اصلی تحلیل یا «ابزارهای تجزیه و تحلیل» اقتصاد را عرضه میکند. ازاین رو این تئوری یکی از مهمترین دروس رشته های اقتصاد و بازرگانی و دروس مشابه است. ");
       }
        imageSlider.setImageList(slideModels);



    }
}