package com.example.bookstore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity2 extends AppCompatActivity {
    private int[] image = {R.drawable.b1,R.drawable.b2,R.drawable.b3,R.drawable.b4};
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        RecyclerView  recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        MyBookData[] myBookData = new MyBookData[]{
          new MyBookData("Name: هنر سینما",R.drawable.b1),
                new MyBookData("Name: دست های آلوده",R.drawable.b2),
                new MyBookData("Name: گزینه اشعار سیاوش کسرایی",R.drawable.b3),
                new MyBookData("Name: تئوریها و مسائل اقتصاد خرد",R.drawable.b4)
        };
        MyBookAdapter myBookAdapter = new MyBookAdapter(myBookData,MainActivity2.this);
        recyclerView.setAdapter(myBookAdapter);
    }
}